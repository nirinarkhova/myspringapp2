package ru.nirinarkhova.springapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.nirinarkhova.springapp.model.Champion;
import ru.nirinarkhova.springapp.service.ChampionService;

import java.util.List;

@Controller
public class ChampionController {

    private final ChampionService championService;

    @Autowired
    public ChampionController(ChampionService championService){
        this.championService = championService;
    }

    @GetMapping("/champions")
    public String findAll(Model model){
        List<Champion> champions = championService.findAll();
        model.addAttribute("champions", champions);
        return "champion-all";
    }

    @GetMapping("/champion-create")
    public String createChampionForm(Champion champion){
        return "champion-create";
    }

    @PostMapping("/champion-create")
    public String createChampion(Champion champion){
        championService.saveChampion(champion);
        return "redirect:/champions";
    }

    @GetMapping("champion-delete/{id}")
    public String deleteChampion(@PathVariable("id") Integer id){
        championService.deleteById(id);
        return "redirect:/champions";
    }

    @GetMapping("/champion-update/{id}")
    public String updateChampionForm(@PathVariable("id") Integer id, Model model){
        Champion champion = championService.findById(id);
        model.addAttribute("champion", champion);
        return"/champion-update";
    }

    @PostMapping("/champion-update")
    public String updateChampion(Champion champion){
        championService.saveChampion(champion);
        return "redirect:/champions";
    }
}
