package ru.nirinarkhova.springapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nirinarkhova.springapp.model.Champion;
import ru.nirinarkhova.springapp.repository.ChampionRepository;

import java.util.List;

@Service
public class ChampionService {

    private final ChampionRepository championRepository;

    @Autowired
    public ChampionService(ChampionRepository championRepository) {
        this.championRepository = championRepository;
    }

    public Champion findById(Integer id){
        return championRepository.findById(id).orElse(null);
    }

    public List<Champion> findAll(){
        return championRepository.findAll();
    }

    public Champion saveChampion(Champion champion){
        return championRepository.save(champion);
    }

    public void deleteById(Integer id){
        championRepository.deleteById(id);
    }
}
