package ru.nirinarkhova.springapp.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Data
@Entity
@Table(name = "champions")
public class Champion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "chorigin")
    private String chorigin;

    @Column(name = "chclass")
    private String chclass;

    @Column(name = "cost")
    private int cost;

}
