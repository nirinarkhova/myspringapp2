package ru.nirinarkhova.springapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nirinarkhova.springapp.model.Champion;

public interface ChampionRepository extends JpaRepository <Champion, Integer>{


}
